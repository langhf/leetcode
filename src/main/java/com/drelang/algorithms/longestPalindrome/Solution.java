package com.drelang.algorithms.longestPalindrome;

abstract class Solution {
    abstract String longestPalindrome(String s);
}