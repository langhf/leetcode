package com.drelang.algorithms.reverseInteger;

public abstract class Solution {
        public abstract int reverse(int x);
}
