package com.drelang.algorithms.validParentheses;

abstract class Solution {
    public abstract boolean isValid(String s);
}
