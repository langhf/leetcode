package com.drelang.algorithms.romanToInteger;

public abstract class Solution {
    public abstract int romanToInt(String s);
}
