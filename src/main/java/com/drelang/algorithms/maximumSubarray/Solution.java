package com.drelang.algorithms.maximumSubarray;

abstract class Solution {
    public abstract int maxSubArray(int[] nums);
}
