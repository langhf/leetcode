package com.drelang.algorithms.mergeKSortedLists;

/**
 * Created by Drelang on 2019/02/07 22:39
 */
public class ListNode {
      int val;
      ListNode next;
      ListNode(int x) { val = x; }
}