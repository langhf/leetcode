package com.drelang.algorithms.linkedListCycle;

/**
 * Created by Drelang on 2019/02/14 21:53
 */
public class ListNode {
    int val;
    ListNode next;
    ListNode(int x) {
        val = x;
        next = null;
    }
}
